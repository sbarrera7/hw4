package com.example.hw4quiz;

class Questions {

    String mQuestions[] = {
            "What type of animal is a seahorse?",
            "Which of the following dogs is the smallest?",
            "What color are zebras?",
            "What existing bird has the largest wingspan?",
            "What is the biggest animal that has ever lived?",
            "What pets do more families own?",
            "What animal lives the longest?",
            "What are female elephants called?",
            "Which of the following animals sleep standing up?",
            "What is the fastest water animal?",
    };

    private String mChoices[][] = {
            {"Crustacean","Arachnid","Fish","Shell"},
            {"Dachshud","Poodle","Pomeranian","Chihuahua"},
            {"White with black stripes","Black with white stripes","Both of the above","None of the above"},
            {"Stork","Swan","Condor","Albatross"},
            {"Blue whale","African elephant","Apatosaurus","Spinosaurus"},
            {"Birds","Cats","Dogs","Horses"},
            {"Ocean quahog", "Red sea urchin","Glalpagos tortois","Rougheye rockfish"},
            {"Mares","Sows","Cows","Dams"},
            {"Gorillas","Flamingos","Camels","Ravens"},
            {"Porpoise","Sailfish","Flying fish","Tuna"}
    };

    private String mCorrectAnswers[]={"Fish","Chihuahua","Black with white stripes","Albatross","Blue whale","Cats","Ocean quahog","Cows","Flamingos","Sailfish"};

    public String getQuestion(int a) {
        String question = mQuestions[a];
        return question;
    }

    public String getChoice1(int a){
        String choice = mChoices[a][0];
        return choice;
    }

    public String getChoice2(int a){
        String choice = mChoices[a][1];
        return choice;
    }

    public String getChoice3(int a){
        String choice = mChoices[a][2];
        return choice;
    }

    public String getChoice4(int a){
        String choice = mChoices[a][3];
        return choice;
    }

    public String getCorrectAnswer(int a){
        String answer = mCorrectAnswers[a];
        return answer;
    }

}
